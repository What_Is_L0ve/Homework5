﻿namespace Common.Dto;

public class PersonDto
{
    public string Name { get; set; } = null!;

    public float Salary { get; set; }
}