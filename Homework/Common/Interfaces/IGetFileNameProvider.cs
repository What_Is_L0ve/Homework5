﻿using Common.Implementations;

namespace Common.Interfaces;

public interface IGetFileNameProvider
{
    public event EventHandler<FileArgs>? FileFound;

    void OnGetFileName(string directory);
}