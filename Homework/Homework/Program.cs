﻿using Common;
using Common.Interfaces;
using Microsoft.Extensions.DependencyInjection;

namespace Homework;

static class Program
{
    private static void Main()
    {
        var provider = DependencyInjection.ConfigureServices();
        var getMaxService = provider.GetRequiredService<IGetMaxEnumerableTestService>();
        getMaxService.Test();
        var getFileNameService = provider.GetRequiredService<IGetFileNameProviderTestService>();
        getFileNameService.Test();
    }
}